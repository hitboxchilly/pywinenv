import argparse
import winreg

from pathlib import Path

def main(argv=None):
    """
    """
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-l', '--list', action='store_true')
    args = parser.parse_args(argv)

    if args.list:
        #hk = winreg.OpenKey(
        #    winreg.HKEY_LOCAL_MACHINE,
        #    'SYSTEM\CurrentControlSet'
        #    '\Control\Session Manager\Environment')
        key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, 'Environment')
        nsubkeys, nvalues, last_modified = winreg.QueryInfoKey(key)
        for i in range(nvalues):
            name, data, type_ = winreg.EnumValue(key, i)
            if name.lower() == 'path':
                paths = list(map(Path, data.split(';')))
                for path in paths:
                    msg = []
                    if not path.exists():
                        msg.append('*')
                    msg.append(path)
                    print(' '.join(map(str, msg)))

if __name__ == '__main__':
    main()
